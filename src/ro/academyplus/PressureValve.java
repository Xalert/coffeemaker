package ro.academyplus;

import java.util.Observable;
import java.util.Observer;

public class PressureValve implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        CoffeeMaker.commenceCoffeemaking(CoffeeMakerAPI.api);
    }
}
