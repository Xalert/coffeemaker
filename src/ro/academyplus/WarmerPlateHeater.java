package ro.academyplus;

import java.util.Observable;
import java.util.Observer;

public class WarmerPlateHeater implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        CoffeeMaker.keepCoffeeWarm(CoffeeMakerAPI.api);
    }
}