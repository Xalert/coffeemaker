package ro.academyplus;

import java.util.Observable;

public class BrewButton extends Observable {
    public void setBrewButtonStatus() {
        setChanged();
        notifyObservers();
    }
}
