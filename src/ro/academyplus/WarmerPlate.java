package ro.academyplus;

import java.util.Observable;

public class WarmerPlate extends Observable {
      public void setWarmerPlateStatus(CoffeeMakerAPI api, int warmerPlateStatus) {
        api.setWarmerState(warmerPlateStatus);
        setChanged();
        notifyObservers();
    }
}
