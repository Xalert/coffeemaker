package ro.academyplus;

public class CoffeeMaker {
    public static void startCoffeeMaker (CoffeeMakerAPI api) {
        BrewButton brewButton = new BrewButton();
        WarmerPlate warmerPlate = new WarmerPlate();
        PressureValve pressureValve = new PressureValve();
        WarmerPlateHeater warmerPlateHeater = new WarmerPlateHeater();
        BoilerHeater boilerHeater = new BoilerHeater();

        brewButton.addObserver(pressureValve);
        brewButton.addObserver(boilerHeater);
        warmerPlate.addObserver(warmerPlateHeater);

        brewButton.setBrewButtonStatus();
    }

    static void brew (CoffeeMakerAPI api) {
        //Check if the CoffeeMaker isn't brewing already
        if (api.getBoilerStatus() == CoffeeMakerAPI.BOILER_OFF){
            return;
        }
        //Check if the CoffeeMaker doesn't spill
        if (api.getWarmerPlateStatus() == CoffeeMakerAPI.WARMER_EMPTY) {
            return;
        }
        //Check that there is water for at least one coffee
        if (api.getBoilerStatus() < 1/12F) {
            return;
        }

        System.out.println("Brew Started!");

        api.setBoilerState(CoffeeMakerAPI.BOILER_ON);
        api.setIndicatorState(CoffeeMakerAPI.INDICATOR_OFF);
        api.setWarmerState(CoffeeMakerAPI.VALVE_CLOSED);

        //Brews coffee as long as there is water
        while (api.getBoilerStatus() > 0) {
        }

        notEnoughWater(CoffeeMakerAPI.api);
        System.out.println("Brew Ended!");
    }

    static void potNotFound (CoffeeMakerAPI api) {
        api.setReliefValveState(CoffeeMakerAPI.VALVE_OPEN);
        api.setBoilerState(CoffeeMakerAPI.BOILER_OFF);
        api.setWarmerState(CoffeeMakerAPI.WARMER_OFF);
        api.setIndicatorState(CoffeeMakerAPI.INDICATOR_ON);
    }

    static void notEnoughWater (CoffeeMakerAPI api) {
        api.setBoilerState(CoffeeMakerAPI.BOILER_OFF);
        api.setIndicatorState(CoffeeMakerAPI.INDICATOR_ON);
        api.setReliefValveState(CoffeeMakerAPI.VALVE_OPEN);
    }

    static void keepCoffeeWarm (CoffeeMakerAPI api) {
        if (api.getWarmerPlateStatus() != CoffeeMakerAPI.POT_EMPTY) {
            potNotFound(api);
        } else {
            api.setWarmerState(CoffeeMakerAPI.WARMER_ON);
        }
    }

    static void commenceCoffeemaking(CoffeeMakerAPI api) {
        if (api.getWarmerPlateStatus() == CoffeeMakerAPI.POT_EMPTY) {
            return ;
        }
        api.setReliefValveState(CoffeeMakerAPI.VALVE_CLOSED);
    }


}
