package ro.academyplus;

import java.util.Observable;
import java.util.Observer;

public class BoilerHeater implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        CoffeeMaker.brew(CoffeeMakerAPI.api);
    }
}
