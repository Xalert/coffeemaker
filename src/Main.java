import ro.academyplus.CoffeeMaker;
import ro.academyplus.CoffeeMakerAPI;

public class Main {
    public static void main(String[] args) {
        CoffeeMaker.startCoffeeMaker(CoffeeMakerAPI.api);
    }
}
